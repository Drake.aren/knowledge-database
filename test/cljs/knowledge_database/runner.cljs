(ns knowledge-database.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [knowledge-database.core-test]))

(doo-tests 'knowledge-database.core-test)
